﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;


public class Player_Controller : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public GameObject spawnable;
    public TextMeshProUGUI timerText;
    private int count;
    private Rigidbody rb;
    private float movementX;
    private float movementY;
    private float time = 0;
    private int minuteCount = 0;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        winTextObject.SetActive(false);
        SetCountText();
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);

        SetTimerText();
    }


    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 100)
        {
            winTextObject.SetActive(true);
        }
    }

    void SetTimerText()
    {
        time = time + Time.deltaTime;
        if (count <= 100)
        {
            if ((int)time % 60 == 0 && (int)time != 0)
            {
                Debug.Log((int)time);
                minuteCount++;
                time = 0;
            }
            timerText.text = "" + minuteCount + ":" + time.ToString("F2");
        }
    }

    void Play(GameObject pS)
    {
        ParticleSystem part = pS.GetComponent<ParticleSystem>();
        part.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            Play(other.gameObject);
            count++;
            SetCountText();
            Vector3 randLoc = new Vector3(Random.Range(-9, 9), spawnable.transform.position.y, Random.Range(-9, 9));
            Instantiate(spawnable, randLoc, spawnable.transform.rotation);
        }
    }
}
